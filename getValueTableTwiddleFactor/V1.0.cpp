#include <stdio.h>
#include <math.h>


typedef   signed char    int8_t;
typedef unsigned char   uint8_t;
typedef          short   int16_t;
typedef unsigned short  uint16_t;
typedef          int     int32_t;
typedef unsigned int    uint32_t;


typedef          long long  int64_t;
typedef unsigned long long uint64_t;

#define Npoint 512
#define FP_PRECISION 9

//#define V1
#ifdef V1

float arr[Npoint];
int16_t arr1[Npoint];
#define pi 3.1415926535897

int main()
{
	int16_t k;
	for(k = 0; k < Npoint; k++)
	{
		arr[k]  = sin(-2.*pi*k/Npoint);
		arr1[k] = (int16_t)(arr[k]*(1<<FP_PRECISION));
	}	
	for(k = 0; k < Npoint; k++)
	{
		printf("%f \t %d \t 0x%x\n", arr[k], arr1[k]), arr1[k]&0xFFFF;
	}	
	FILE *fp;
	fp = fopen("fix_Tab.txt", "w");
	//fprintf(fp, "floating point:\t\tint16 :\n\n");
	for(k = 0; k < Npoint; k++)
	{
		if((k != 0) && (k%8 == 0)) fprintf(fp, "\n");
		fprintf(fp, "0x%x,\t", 0xffff&arr1[k]);		
	}
	fclose(fp);
	return 0;
}
#else
#define PI      3.1415926535897
int16_t Table[512];

inline int16_t __MUL_Q15(int16_t A, int16_t B)
{
	return (int16_t)(((int32_t)(A) * (int32_t)(B)) >> FP_PRECISION);
}

int main()
{
	float WR, WI;
	uint8_t log2N = 9;
//	for(int step = 1; step <= log2N; step++)
//	{
//		int temp3 = 1<<(step -1); 
//		for(int k = 0; k < temp3; k++)
//		{
//			int temp = 1<<(step);
////			WR = cosTable[512*k/temp]/(1<<8);//cos(-2.*PI*k/temp);
////			WI = sinTable[512*k/temp]/(1<<8);//sin(-2.*PI*k/temp);
//			WR = cos(-2.*PI*k/temp);
//			WI = sin(-2.*PI*k/temp);
//			Table[512*k/temp] = (int16_t)(WI*(1<<FP_PRECISION));
//		}
//	}
	for(int j = 0; j < Npoint; j++) 
	{
		WR = cos(2.*PI*j/(Npoint-1));
		Table[j] = (int16_t)(WR*(1<<FP_PRECISION));	
		Table[j] = (int16_t)(0.54*(1<<FP_PRECISION)) - __MUL_Q15((int16_t)(0.46*(1<<FP_PRECISION)), Table[j]);
	}
	for(int i = 0; i < Npoint; i++) 
	{
		if((i != 0) && (i%8 == 0)) printf("\n");
		printf("0x%x,\t", 0xffff&Table[i]);
	}
	return 0;
}
#endif

