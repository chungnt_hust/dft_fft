#include <stdio.h>
#include <math.h>
#include<time.h>

#define PI      3.1415926535897
#define N_point 512

float RE[N_point];
float IM[N_point];

void get_sample(float *R, float *I, int N)
{
	int i;
	for(i = 0; i < N; i++)
	{
		R[i] = 3.0 + 1.0*sin(2.*PI*i);
		I[i] = 0;
	}
}

void Hamming(float *R, float *I, int N)
{
    double omega = 2.0 * PI / (N-1);

    // owv[i].Re = real number (raw wave data)
    // owv[i].Im = imaginary number (0 since it hasn't gone through FFT yet)
    for (int i = 0; i < N; i++)
        // Translated from c++ sample I found somewhere
        R[i] = (0.54 - 0.46 * cos(omega * (i))) * R[i]; 
}

void DFT(float *R, float *I, int N)
{
	int k, n;
	float bn = 0;
	float RE_buf[N];
	float IM_buf[N];
	
	for(k = 0; k < N; k++)
	{
		RE_buf[k] = 0;
		IM_buf[k] = 0;
	}
	
	for(k = 0; k < N; k++)
	{
		for(n = 0; n < N; n++)
		{
			bn = (2*PI*k*n)/N;
			RE_buf[k] += R[n]*cos(bn);
			IM_buf[k] += R[n]*sin(-bn);
		}
	}
	
	for(k = 0; k < N_point; k++)
	{
		R[k] = RE_buf[k];
		I[k] = IM_buf[k];
	}
}

int main()
{
	int x;
	get_sample(RE, IM, N_point);	
	DFT(RE, IM, N_point);
	Hamming(RE, IM, N_point);
	for(x = 0; x < N_point; x++)
	printf("re[%d] = %6.3f \t im[%d] = %6.3f \t mag[%d] = %6.3f\n", x, RE[x], x, IM[x], x, sqrt(RE[x]*RE[x] + IM[x]*IM[x]));
	return 0;
}
